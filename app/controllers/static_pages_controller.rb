class StaticPagesController < ApplicationController
	include ApplicationHelper
  def contacts
  end

  def home
  end

  def help
  end
  
  def about
  end
end
